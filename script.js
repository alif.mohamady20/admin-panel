getAll();
function getAll() {
  callAjax(
    "http://5e300a69576f9d0014d63aac.mockapi.io/user",
    "",
    "GET",
    resultGetAll
  );
}
function resultGetAll(data) {
  $(data).each(function() {
    $("#listUser").append(
      "<ul>" +
        "<li>" +
        '<div class="mt-5">' +
        this.username +
        "</div>" +
        '<button class="btn btn-danger mr-5" data-id="' +
        this.id +
        '" onclick="userRemove(this)">delete</button>' +
        '<button class="btn btn-primary" data-edit="' +
        this.id +
        '" onclick="userEdit(this)">edit</button>' +
        "</li>" +
        "</ul>"
    );
  });
}
function addUser() {
  var username = $("#userName").val(),
    userEmail = $("#emailUser").val(),
    user_id = $("#userId").val(),
    pass = $("#password").val(),
    phone_num = $("#phoneNumber");

  if (username !== "") {
    if (userEmail !== "") {
      if (user_id !== "") {
        if (pass !== "") {
          if (phone_num !== "") {
            const body = {
              username: username,
              email: userEmail,
              id: user_id,
              password: pass,
              phonenum: phone_num
            };
            callAjax(
              "http://5e300a69576f9d0014d63aac.mockapi.io/user",
              body,
              "POST",
              addUser_res
            );
            console.log(body);
          } else {
          }
        } else {
        }
      } else {
        alert("ایدی خالی است.");
      }
    } else {
      alert("ایمیل خالی است.");
    }
  } else {
    alert("نام کاربری خالی است.");
  }
}
function addUser_res(data) {
  console.log(data);
}
function userRemove(element) {
  var id = $(element).attr("data-id");
  console.log(id);
  callAjax(
    "http://5e300a69576f9d0014d63aac.mockapi.io/user/" + id + "",
    "",
    "DELETE",
    resultDeleteUser
  );
}
function resultDeleteUser(data) {
  alert("حذف با موفقیت انجام شد.");
  getAll();
}
function userEdit(element) {
  var id = $(element).attr("data-edit");
  callAjax(
    "http://5e300a69576f9d0014d63aac.mockapi.io/user/" + id + "",
    "",
    "PUT",
    resultEditUser
  );
}
function resultEditUser(data) {
  // alert('ویرایش شد');
  $("#userName").val(data.username);
  $("#emailUser").val(data.email);
  $("#userId").val(data.id);
  $("#password").val(data.password);
  $("#phoneNumber").val(data.phonenum);
}

function callAjax(api, body, type, callbackFunc) {
  $.ajax({
    url: api,
    type: type,
    data: body,
    dataType: "json",
    contentType: "application/json",
    success: function(body) {
      if (typeof callbackFunc === "function") {
        callbackFunc(body);
      }
    }
  });
}
